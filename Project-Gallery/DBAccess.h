#pragma once

#include "sqlite3.h"
#include "IDataAccess.h"

class DBAccess : public IDataAccess
{
public:
	DBAccess() = default;
	virtual ~DBAccess() = default;

	// album related
	std::list<Album> getAlbums() override;
	std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void closeAlbum(Album& pAlbum) override;
	void printAlbums() override;
	auto getPicturesByAlbumName(const std::string& aName) const -> std::list<Picture>;

	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	std::list<int> getAllTags(const int pictureId) const;
	
	// user related
	std::list<User> getUsers() const;
	void printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;
	void deleteUserTags(const User& user) const;

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;
	auto open() -> bool override;
	void close() override;
	void clear() override;
	
private:
	sqlite3* _db;
	std::string _dbFilename = "galleryDB.sqlite";
	
	std::list<Album> _albums;
	std::list<User> _users;

	auto getPictureId(const std::string& pName) const -> int;
	auto getAlbumId(const std::string& aName) const -> int;
};

