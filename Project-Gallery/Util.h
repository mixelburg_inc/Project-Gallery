#pragma once

#include <Windows.h>

STARTUPINFOA info = { sizeof(info) };
PROCESS_INFORMATION processInfo;

BOOL WINAPI ConsoleHandler(DWORD CEvent)
{
    char mesg[128];

    switch (CEvent)
    {
    case CTRL_C_EVENT:
        TerminateProcess(processInfo.hProcess, 1);
        break;
    }
    return TRUE;
}

std::string getCurrentDirectory()
{
    char buffer[MAX_PATH];
    GetModuleFileNameA(NULL, buffer, MAX_PATH);
    std::string::size_type pos = std::string(buffer).find_last_of("\\/");

    return std::string(buffer).substr(0, pos);
}


#pragma once
